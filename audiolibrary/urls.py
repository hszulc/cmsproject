from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^detail/(?P<audiobook_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^detail/(?P<audiobook_id>[0-9]+)/download/$', views.download, name='download'),
    url(r'^all/$', views.all, name='all'),
    url(r'^category/(?P<audiobook_category_id>[0-9]+)/$', views.category, name='category'),
    url(r'^contact/$', views.contact, name='contact'),
    url(r'^contact_send/$', views.contact_send, name='contact_send'),
    url(r'^newsletter/$', views.newsletter, name='newsletter'),
    #url(r'^contact_successful/$', views.contact_successful, name='contact_successful'),
    url(r'^$', views.index, name='index'),
]