# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.template import loader
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone

from ipware import get_client_ip

from audiolibrary.models import Audiobook, AudiobookCategory, VisitingHistory, EmailList, Messages

def index(request):
    log_visit(request)

    newest_audiobooks_list = Audiobook.objects.order_by('-pub_date')[:8]
    audiobook_categories_list = AudiobookCategory.objects.all()
    context = {
        'newest_audiobooks_list': newest_audiobooks_list,
        'audiobook_categories_list': audiobook_categories_list,
        'visit_count': visit_count(),
        'last_visit': last_visit(),
    }
    return render(request, 'audiolibrary/index.html', context)

def detail(request, audiobook_id):
    log_visit(request)

    audiobook = get_object_or_404(Audiobook, pk = audiobook_id)
    return render(request, 'audiolibrary/detail.html', {'audiobook': audiobook, 'visit_count': visit_count(), 'last_visit': last_visit()})

def download(request, audiobook_id):
    log_visit(request)

    ab = get_object_or_404(Audiobook, pk = audiobook_id)
    filename = ab.file.name.split('/')[-1]
    response = HttpResponse(ab.file, content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename=%s' % filename

    return response

def category(request, audiobook_category_id):
    log_visit(request)

    audiobook_category = get_object_or_404(AudiobookCategory, pk=audiobook_category_id)
    audiobook_list = Audiobook.objects.filter(category_id=audiobook_category_id)
    return render(request, 'audiolibrary/category.html', {'audiobook_category': audiobook_category, 'audiobook_list': audiobook_list, 'visit_count': visit_count(), 'last_visit': last_visit()})

def all(request):
    log_visit(request)

    audiobook_category = "All"
    audiobook_list = Audiobook.objects.all
    return render(request, 'audiolibrary/category.html', {'audiobook_category': audiobook_category, 'audiobook_list': audiobook_list, 'visit_count': visit_count(), 'last_visit': last_visit()})

def contact(request):
    log_visit(request)

    return render(request, 'audiolibrary/contact.html', {'visit_count': visit_count(), 'last_visit': last_visit()})

def contact_send(request):
    email = request.POST['email']
    message = request.POST['message']
    m = Messages(email=email, message=message)
    m.save()
    return render(request, 'audiolibrary/contact_successful.html', {'visit_count': visit_count(), 'last_visit': last_visit()})

def newsletter(request):
    add_to_newsletter(request.POST.get('email', False))
    previous = request.POST.get('next', '/')
    return HttpResponseRedirect(previous + '?nl=1')


def add_to_newsletter(address):
    if address:
        email = EmailList(email=address)
        email.save()

def log_visit(request):
    ip, routing = get_client_ip(request)
    visit = VisitingHistory(ip=ip, timestamp=timezone.now())
    visit.save()

def last_visit():
    return VisitingHistory.objects.latest('timestamp').timestamp

def visit_count():
    return VisitingHistory.objects.count()