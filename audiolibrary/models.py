# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class AudiobookCategory(models.Model):
    name = models.CharField(max_length=200)
    parent = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'category'
        verbose_name_plural = 'categories'

class Audiobook(models.Model):
    name = models.CharField(max_length=200)
    author = models.CharField(max_length=200)
    reader = models.CharField(max_length=200, verbose_name="narrated by")
    pub_date = models.DateField('date published')
    length = models.TimeField('track length')
    file_format = models.CharField(max_length=10)
    file = models.FileField()
    category = models.ForeignKey(AudiobookCategory)
    thumbnail = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class VisitingHistory(models.Model):
    ip = models.CharField(max_length=50)
    timestamp = models.DateTimeField()

    def __str__(self):
        return self.ip

    class Meta:
        verbose_name = 'visit'
        verbose_name_plural = 'visits log'

class EmailList(models.Model):
    email = models.EmailField()

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = 'email'
        verbose_name_plural = 'emails'

class Messages(models.Model):
    email = models.EmailField()
    message = models.TextField(max_length=1000)

    def __str__(self):
        return self.message

    class Meta:
        verbose_name = 'message'
        verbose_name_plural = 'messages'