# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from audiolibrary.models import Audiobook, AudiobookCategory, VisitingHistory, EmailList, Messages

class AudiobookAdmin(admin.ModelAdmin):
    list_display = ('name', 'author', 'category', 'pub_date')
    list_filter = ['pub_date']
    search_fields = ['name']

class VisitingHistoryAdmin(admin.ModelAdmin):
    list_display = ('ip', 'timestamp')
    search_fields = ['ip']

class MessagesAdmin(admin.ModelAdmin):
    list_display = ('email', 'message')

admin.site.register(Audiobook, AudiobookAdmin)
admin.site.register(AudiobookCategory)
admin.site.register(VisitingHistory, VisitingHistoryAdmin)
admin.site.register(EmailList)
admin.site.register(Messages, MessagesAdmin)

# Register your models here.
